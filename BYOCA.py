import pyglet
import math 
from pyglet.gl import *
from pyglet.window import key, mouse
from random import choice#, random

#Dimensions of the screen
window_width  = 500
window_height = 500
cell_size = 100
sim_cell_size = 10

columns = window_width / cell_size
rows = window_height / cell_size

sim_columns = window_width / sim_cell_size
sim_rows = window_height / sim_cell_size

global cells, sim_cells, neighborhood, function, state, input_num

cells = [[False] * columns for i in range(rows)]
sim_cells = [[0] * sim_columns for i in range(sim_rows)]
neighborhood = []
function = []
state = 'choose_neighborhood'

win = pyglet.window.Window(window_width, window_height)


def rectangle(x1, y1, x2, y2):
    pyglet.graphics.draw(4, GL_QUADS, ('v2f', (x1, y1, x1, y2, x2, y2, x2, y1)))
    
def define_function(neighborhood):
    function = []
    for i in range(len(neighborhood)+1):
        # prompt: if sum equals i, should CA output 0 or 1 (or input or not input) ?
        print 'If sum equals ' + str(i) + ',\n define action of CA (0,1, in, nin):\n'
        action = input()
        if action in [0,1,'in','nin']:
            function.append[a] # where a = 0, 1, in, nin
    return function

def get_label(m,fs):
    return pyglet.text.Label(m, font_name='Comic Sans', font_size=fs, x=win.width//6, y=win.width//2, multiline=True, width=win.width) #anchor_x='center', anchor_y='center')

def seed_grid():
    # populate the grid at random
    global sim_cells
    for y in range(sim_rows):
        for x in range(sim_columns):
            sim_cells[y][x] = choice((0,0,0,0,1,1,1))

# (3) run user-created CA
def simulate():
    # evolve the grid
    global sim_cells
    
    # empty working grid
    working_grid = [[0]*sim_columns for i in range(sim_rows)]
    
    for y in range(sim_rows):
        for x in range(sim_columns):
            total = 0
            
            for a,b in neighborhood:
                ny, nx = (y+a)%sim_rows, (x+b)%sim_columns  # perodic boundary conditions
                total += sim_cells[ny][nx]
            
            if function[total] == 0:
                working_grid[y][x] = 0
                    
            elif function[total] == 1:
                working_grid[y][x] = 1
                    
            elif function[total] == 'in':
                working_grid[y][x] = sim_cells[y][x]
        
            elif function[total] == 'nin':
                if sim_cells[y][x] == 0:
                    working_grid[y][x] = 1
                else:
                    working_grid[y][x] = 0
                    
            else:
                # error handling for now
                print 'invalid input!'
                

    # copy working_grid onto sim_cells
    sim_cells = working_grid
    
def draw_grid(r, c, cell_sz):
    glColor4f(0.5, 0.5, 0.5, 1.0)
    #Horizontal lines
    for i in range(r):
        pyglet.graphics.draw(2, GL_LINES, ('v2i', (0, i * cell_sz, window_width, i * cell_sz)))
    #Vertical lines
    for j in range(c):
        pyglet.graphics.draw(2, GL_LINES, ('v2i', (j * cell_sz, 0, j * cell_sz, window_height)))


#@win.event
def draw():
    
    # Clear buffers
    glClear(GL_COLOR_BUFFER_BIT)
    
    if state == 'choose_neighborhood':
        draw_grid(rows, columns, cell_size)
        
        glColor4f(0.0, 1.0, 1.0, 1.0)
        
        for f in range(len(cells)):
            current_row = cells[f]
            
            for c in range(len(current_row)):
                if current_row[c]:
                    rectangle(c * cell_size, f * cell_size,
                              c * cell_size + cell_size, f * cell_size + cell_size)
    
    elif state == 'design_function':
        string = 'If sum of neighborhood cells equals ' + str(input_num) + ', update current cell to ???\n ' + 'Enter Z to give cell a value of 0,\n Enter 1 to give cell a value of 1,\n Enter 2 to keep current value,\n Enter 3 to make opposite of current value' 
        
        label = get_label(string, 18)
        label.draw()       
            
    elif state == 'simulate':
        draw_grid(sim_rows, sim_columns, sim_cell_size)
        
        glColor4f(0.0, 1.0, 1.0, 1.0)
        
        for f in range(len(sim_cells)):
            current_row = sim_cells[f]
            
            for c in range(len(current_row)):
                if current_row[c]:
                    rectangle(c * sim_cell_size, f * sim_cell_size,
                              c * sim_cell_size + sim_cell_size, f * sim_cell_size + sim_cell_size)

@win.event
def on_mouse_press(x, y, button, modifiers):
    if button == mouse.LEFT:
        if x >= 0 and x <= window_width:
            if y >= 0 and y <= window_height: 
                y_cell_num = int(math.floor(y/cell_size))
                x_cell_num = int(math.floor(x/cell_size))
                
                cells[y_cell_num][x_cell_num] = not cells[y_cell_num][x_cell_num] 
                
                # change to relative coords by subtracting vector (2,2) since this is the center of a 5 x 5 grid
                
                # if cell is selected add to neighborhood
                if cells[y_cell_num][x_cell_num] and (y_cell_num-2, x_cell_num-2) not in neighborhood:
                    neighborhood.append( (y_cell_num-2, x_cell_num-2)  )
                
                # if cell was unselected remove from neighborhood
                if not cells[y_cell_num][x_cell_num] and (y_cell_num-2, x_cell_num-2) in neighborhood:
                    del neighborhood[neighborhood.index((y_cell_num-2, x_cell_num-2))]
                    
@win.event
def on_key_press(symbol, modifiers):
    global state, neighborhood, cells, paused, seeded, input_num, function
    if symbol == key.X:
        if state == 'choose_neighborhood':
            #print neighborhood
            state = 'design_function'
            input_num = 0
            function = [0 for i in range(len(neighborhood)+1)]

        elif state == 'simulate':
            cells = [[False] * columns for i in range(rows)]
            neighborhood = []
            state = 'choose_neighborhood'
            # make seeded global and reset
            # close window
            #win.close()
    if state == 'design_function':
        if symbol == key.Z:
            function[input_num] = 0
            input_num+=1
            
        elif symbol == key._1:
            function[input_num] = 1
            input_num+=1
        
        elif symbol == key._2:
            function[input_num] = 'in'
            input_num+=1
            
        elif symbol == key._3:
            function[input_num] = 'nin'
            input_num+=1
            
            
    if symbol == key.SPACE:
        # pause/unpause simulation
        paused = not paused

fps_limit = 20
pyglet.clock.set_fps_limit(10)

paused = False
while not win.has_exit:

    win.dispatch_events()
    dt = pyglet.clock.tick()
    
    # set title framerate
    if state == 'choose_neighborhood':
        win.set_caption('Choose Neighborhood')
    elif state == 'design_function':
        win.set_caption('Designing Function...')
    elif state == 'simulate':
        win.set_caption('Running CA...')

    # clear
    #glClear(GL_COLOR_BUFFER_BIT)
    #glLoadIdentity()
    
    if state == 'simulate' and not paused: 
        if not seeded:
            seed_grid()
            seeded = True
        simulate()
        
    if state == 'design_function' and input_num >= len(neighborhood)+1:
        state = 'simulate'
        seeded = False 
        
    draw()
    win.flip()